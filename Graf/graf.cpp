#include "cstdlib"
#include "iostream"
#include <vector>
#include <deque>


//	0-1-2
//	|/	|
//	3---4
//	 \ /
//	  5   6

//[1,3]
//[0,2,3]
//[1,4]
//[0,1,4,5]
//[2,3,5]
//[3,4]


template<typename T>
class graf
{
private:
	class Node{
	public:
		std::vector edges;
		T value;
		bool checked;
		void connect(Node* edge){
			edges.push_back(edge);
		}
		void disconnect(Node* edge){
			for (auto i : edges){
				if (*i == edge){
					edges.erase(i);
				}
			}
		}

	};

	std::vector< std::vector<int> > data {
		{1,3},
		{0,2,3},
		{1,4},
		{0,1,4,5},
		{2,3,5},
		{3,4},
		{0}
	};
public:
	void add_edge(Node* first, Node* second){
		first->connect(second);
		second->connect(first);
	}

	void projdi_hloubka(int vrchol,int barva){
		std::vector<int> zasobnik{vrchol};		//zásobník vrcholů
		std::vector<int> oznacen(data.size());	//seznam všech vrcholů - kterou barvou jsou obarveni
		oznacen[vrchol]=barva;
		while (zasobnik.size()>0){				//dokud v zásobníku něco je
			int v = zasobnik.back();			//vezmi ze zásobníku
			zasobnik.pop_back();				// (když se ze zásobníku vyndavá, vymaže se)
			for (int x = 0; x < data[v].size(); ++x){	//podívej se kam vrchol v zásobníku míří
				if (oznacen[ data[v][x] ]==0){
					oznacen[ data[v][x] ]=barva;		//pokud můžeš, obarvi
					zasobnik.push_back(data[v][x]);		//a vlož do zásobníku
				}
			}
		}
		for (int x = 0; x < oznacen.size(); ++x){	//vypiš, všechny body - jak jsou obarvené
			std::cout << oznacen[x];
		}
		std::cout << std::endl;
	}

	void projdi_sirka(int vrchol,int barva){
		std::deque<int> fronta{vrchol};		//fronta vrcholů
		std::vector<int> oznacen(data.size());	//seznam všech vrcholů - kterou barvou jsou obarveni
		oznacen[vrchol]=barva;
		while (fronta.size()>0){				//dokud v frontě něco je
			int v = fronta.front();			//vezmi ze fronty
			fronta.pop_front();				// (když se ze frontau vyndavá, vymaže se)
			for (int x = 0; x < data[v].size(); ++x){	//podívej se kam vrchol v frontě míří
				if (oznacen[ data[v][x] ]==0){
					oznacen[ data[v][x] ]=barva;		//pokud můžeš, obarvi
					fronta.push_front(data[v][x]);		//a vlož do fronty
				}
			}
		}
		for (int x = 0; x < oznacen.size(); ++x){	//vypiš, všechny body - jak jsou obarvené
			std::cout << oznacen[x];
		}
		std::cout << std::endl;
	}
};

int main(int argc, char const *argv[])
{
	graf G;
	G.projdi_hloubka(0,1);
	G.projdi_sirka(0,1);
	return EXIT_SUCCESS;
}