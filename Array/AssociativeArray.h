#include <vector>
using namespace std;

template<typename KEY, typename VALUE>
class AssociativeArray {
public:
    class Item {
    public: 
        KEY key;
        VALUE value;
        Item(KEY key, VALUE value):
            key(key),
            value(value){
        }
    };
    vector<Item> data;

    AssociativeArray(){}
    AssociativeArray(AssociativeArray const & other);

    void insert(KEY const & key, VALUE const & value){
        if (!has(key)){
        data.push_back( Item(key, value) );
        }
        else {
            throw "Key already exists";
        }
    }

    void erase(KEY const & key){
        for(auto i = data.begin(); i != data.end(); ++i){
            if (i->key == key){
                data.erase(i);
                return;
            }
        }
    }
    bool has(KEY const & key) const{
        for(Item i : data){
            if (i.key == key){
                return true;
            }
        }
        return false;
    }
    VALUE & operator[](KEY const & index){
        for (Item & item : data) {
            if (item.key == index) {
                return item.value;
            }
        }
    }
    VALUE const & operator[](KEY const & index) const{
        for (Item item : data) {
            if (item.key == index) {
                return item.value;
            }
        }
    }

    
    class Iterator {

    private:
        typename std::vector<Item>::iterator vectorIter;

    public:

        Iterator(typename std::vector<Item>::iterator value):   //typename tam je aby compiler věděl že to je typ, ne value
            vectorIter(value) {
        }

        Item & operator * () {
            return *vectorIter;
        }
        Item const & operator * () const {
            return *vectorIter;
        }

        Iterator & operator ++() {
            ++vectorIter;
            return *this;
        }

        Iterator operator ++(int) {
            auto result = Iterator(vectorIter);
            ++vectorIter;
            return result;
        }

        bool operator == (Iterator const & other) const {
            return (vectorIter == other.vectorIter);
        }
        bool operator != (Iterator const & other) const {
            return (vectorIter != other.vectorIter);
        }

        Item * get() {
            return vectorIter;
        }
    };

    Iterator begin(){
        return Iterator(data.begin());
    }
    Iterator end(){
        return Iterator(data.end());
    }
 
};