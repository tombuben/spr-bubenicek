#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
 
using namespace std;

template<typename T>
class Array {
private:
    T * data;
    unsigned length;
    unsigned capacity;
 
    void grow(int newCapacity) {
        T * newData = new T[newCapacity];
        for (unsigned i = 0; i < capacity; ++i)
            newData[i] = data[i];
        capacity = newCapacity;
        delete [] data;
        data = newData;
    }
 
public:
	class Iterator {
    	public:
        	Iterator(T * value):
        		pointer(value){
        	}
         
 
	        T & operator * (){
	        	return *pointer;
	        }
    	    T const & operator * () const{
    	    	return *pointer;
    	    }
 
 
        	Iterator & operator ++(){
        		pointer = pointer + 1;
        		return *this;
        	}
        	Iterator operator ++(int){
        		Iterator newiterator = *this;
        		++pointer;
        		return newiterator;
        	}
         
        	bool operator == (Iterator const & other) const{
        		return pointer == other.pointer;
       		}
        	bool operator != (Iterator const & other) const{
        		return pointer != other.pointer;
        	}
    	private:
			T * pointer;
    };

    Iterator begin(){
    	return Iterator(data);
    }
    Iterator end(){
    	return Iterator(data+length);
    }

    Array():
        data(new T[10]),
        length(0),
        capacity(8) {
    }
 
    Array(int length) :
        data(new T[length]),
        length(length),
        capacity(length) {
    }
 
    ~Array() {
        delete[] data;
    }
 
    T & get(unsigned index) {
        return data[index];
    }
 
    void set(unsigned index, T item) {
        data[index] = item;
    }
 
    void append(T item) {
        if (++length == capacity)
            grow(capacity * 2);
        set(length -  1, item);
    }

    void remove(unsigned index) {
        while (index < length){
            data[index] = data[index+1];
            ++index;
        }
        length = length - 1;
    }
 
    unsigned size() {
        return length;
    }
};