#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>

#include "AssociativeArray.h"

using namespace std;

int main(int argc, char const ** argv) {

    AssociativeArray<const char*, int> array;
    array.insert("bread",100);
    array.insert("butter",1);
    array.insert("jam",100);
    cout << ".has() is working: " << array.has("bread") << endl;
    cout << ".has() is not working: " << array.has("breadly") << endl;
    array.erase("bread");
    cout << ".erase() is not working: " << array.has("bread") << endl;
    cout << "[] is working: " << array["butter"] << endl;
    array["butter"] = 2;
    cout << "[] can change: " << array["butter"] << endl;

    cout << "---- array contents ----" << endl;
    for(auto x : array){
    	cout << x.key << ":" << x.value << endl;
    }
}