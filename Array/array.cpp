#include "array.h"

using namespace std;

int main(int argc, char const ** argv) {

    Array<int> x;

    int counter1 = 0;
    while (counter1<15){
    	x.append(++counter1);
    }
//    // ukazat ze vase pole nebo list jdou projit:
    int counter2 = 5;
    for (Array<int>::Iterator i = x.begin(); i != x.end(); ++i) {
        // zmenit hodnotu
        *i = ++counter2;
        cout << *i << endl;
    }

    cout << "----" << endl;

    int counter3 = 0;

    for (int i : x) {
        // ukazat ze hodnota zmenit nejde a vysvetlit proc v komentari
        i = i-5;
        cout << i << "(iterator value) vs " << x.get(counter3++) << "(real value)" << endl;
        
        // proměnná i se zkopíruje, změnění té hodnoty nezmění původní hodnoty v poli
    }

    cout << "----" << endl;

    int counter4 = 0;
    for (int & i : x) {
        // zmenit hodnotu a vypsat a vysvetlit proc ted vypsat jde
        i = i+10;
        cout << i << "(iterator value) == " << x.get(counter4++) << "(real value)" << endl;

        // místo kopírování to použije tu referenci
    }


    cout << "----" << endl;
    x.remove(4);
    for(int i:x){
        cout << i << endl;
    }
}