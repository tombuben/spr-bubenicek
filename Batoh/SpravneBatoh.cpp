#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
 
using namespace std;
 
class Item {
public:
    double cost;
    double weight;
    double ratio; // cena děleno váha
 
    Item() :
        cost(0),
        weight(0),ratio(0) {
    }
 
    Item(double cost, double weight):
        cost(cost),
        weight(weight) {
        ratio = 0;
    }
};

template<typename T>
class Array {
private:
    T * data;
    unsigned length;
    unsigned capacity;
 
    void grow(int newCapacity) {
        T * newData = new T[newCapacity];
        for (unsigned i = 0; i < capacity; ++i)
            newData[i] = data[i];
        capacity = newCapacity;
        delete [] data;
        data = newData;
    }
 
public:
    Array():
        data(new T[10]),
        length(0),
        capacity(8) {
    }
 
    Array(int length) :
        data(new T[length]),
        length(length),
        capacity(length) {
    }
 
    ~Array() {
        delete[] data;
    }
 
    T & get(unsigned index) {
        return data[index];
    }
 
    void set(unsigned index, T item) {
        data[index] = item;
    }
 
    void append(T item) {
        if (++length == capacity)
            grow(capacity * 2);
        set(length -  1, item);
    }
 
    unsigned size() {
        return length;
    }
};
 

class Knapsack {
private:
    Array<Item> items;
    bool * bestResult = nullptr; //ukazuje na pole s nejlepšími řešeními
    double bestCost = 0;
    double weight; //kapacita
 
    void bestBruteForce(double remaining, double cost,  int index, bool * solution) {
        if (index == items.size()) {
            checkBest(solution, cost);
        } else {
            Item i = items.get(index);
            if (i.weight <= remaining) { //pokud se tam věc vejde, tak zkus jí tam přidat
                solution[index] = true;
                bestBruteForce(remaining - i.weight, cost + i.cost, index + 1, solution);
            }
            solution[index] = false;
            bestBruteForce(remaining, cost,  index + 1, solution); //a ještě zkus kdyby se tam nevešla
        }
    }
 
    bool * arrayCopy(bool * source, unsigned size) {
        bool * result = new bool[size];
        for (unsigned i = 0; i < size; ++i)
            result[i] = source[i];
        return result;
    }
 
    void checkBest(bool * solution, double cost) {
        if (cost > bestCost) {
            bestCost = cost;
            delete [] bestResult;
            bestResult = arrayCopy(solution, items.size());
        }
    }


    bool allItems(bool * solution){
        for(int i = 0; i < items.size(); ++i){
            if(solution[i]==false){
                return true;
            }
        }
        return true;
    }
 
public:
    Knapsack(double weight) :
        weight(weight) {
 
    } //Nastaví kapacitu
 
    Knapsack(){
        weight = 0;
    }
    void setWeight(double weight){
        this->weight = weight;
    }

    void addItem(double cost, double weight) {
        items.append(Item(cost, weight));
    } //Přidá věc
 
    bool * bruteForce() {
        bool * solution = new bool[items.size()];
        for (unsigned int i = 0; i < items.size(); ++i)
            solution[i] = false;
        bestResult = arrayCopy(solution, items.size());
        bestBruteForce(weight, 0, 0, solution);
        delete [] solution;
        return bestResult;
    }
 
    unsigned numItems() {
        return items.size();
    }

    bool * greedy() {
        for (int i = 0; i < items.size(); ++i){
            items.get(i).ratio = items.get(i).cost/items.get(i).weight;
        }

        bool * solution = new bool[items.size()];
        for (int i = 0; i < items.size(); ++i)
            solution[i] = false;
        bestResult = arrayCopy(solution, items.size());
        double highestValue;
        int highestIndex;
        double currentWeight = 0; //jak je zaplněný batoh
        while (currentWeight<weight or !allItems(solution)){ //ber dokud můžeš
            highestValue = 0;
            for(int i = 0; i < items.size(); ++i){
                if  (solution[i]==false && items.get(i).ratio>highestValue){
                        highestValue = items.get(i).ratio;
                        highestIndex = i;
                }
            }
            solution[highestIndex]=true;//tak tam dej to, co má nejvyšší poměr cena/výkon
            currentWeight=currentWeight+items.get(highestIndex).weight;
        }
        if (currentWeight>weight) { //pokud si vzal moc, vyhoď to cos vzal naposled (to má nejhorší ratio a zabírá to to extra místo)
            solution[highestIndex]=false;
        }
        bestResult = arrayCopy(solution, items.size());
        return bestResult;
    };

    double getValue(bool * solution){
        double currentValue = 0;
        for (int i = 0; i < items.size(); ++i){
            if (solution[i] == true){
                currentValue = currentValue + items.get(i).cost;
            }
        }
        return currentValue;
    }
 
};
 
int main(int argc, char const ** argv) {
    cout << "Random(1), Manual(2): ";
    double choice;
    cin >> choice;

    double weight;
    unsigned numItems;
    Knapsack k;
    if (choice == 2){
        cout << "Knapsack weight: ";
        cin >> weight;
        cout << "Number of items: ";
        cin >> numItems;
        k.setWeight(weight);
        while (numItems > 0) {
            double cost;
            double weight;
            cout << "Cost: ";
            cin >> cost;
            cout << "Weight: ";
            cin >> weight;
            k.addItem(cost, weight);
            --numItems;
        }
    }
    else{
        cout << "Number of random items: ";
        cin >> numItems;
        weight = 3*numItems;
        cout << "weight: " << weight << endl;
        k.setWeight(weight);
        for (int i=0; i<numItems; i++){
            double cost = rand() % 10+1;
            double weight = rand() % 10+1;
            k.addItem(cost, weight);
            if (i%1000==0){
                cout << ".";
            }
        }
    }
    clock_t start;
    double duration;
    cout << endl <<"starting greedy calculations"<< endl;
    
    cout << "greedy: ";
    start = clock();
    bool * solutionGreedy = k.greedy();
    duration = clock()-start;
    if (numItems <= 1000){
        for (unsigned i = 0; i < k.numItems(); ++i)
            if (solutionGreedy[i])
                cout << "1";
            else
                cout << "0";
    }
    else{
        cout << "long, basically useless string";
    }
    cout << endl;
    cout << "greedy took " << duration/CLOCKS_PER_SEC << " seconds. (" << CLOCKS_PER_SEC << "CPS) With the value of " << k.getValue(solutionGreedy) << endl;
    if (numItems<35){
        cout << "starting bruteForce calculations" << endl;
        cout << "bruteForce: ";
        start = clock();
        bool * solutionBrute = k.bruteForce();
        duration = clock()-start;
        for (unsigned i = 0; i < k.numItems(); ++i)
            if (solutionBrute[i])
                cout << "1";
            else
                cout << "0";
        cout << endl;
        cout << "bruteForce took " << duration/CLOCKS_PER_SEC << " seconds. (" << CLOCKS_PER_SEC << "CPS) With the value of " << k.getValue(solutionBrute) << endl;
    }
    else
        cout << "bruteforce would take too long, F U" << endl;
    return EXIT_SUCCESS;
}