#include <iostream>
#include <stdio.h>



int batoh(int pocet, int cena[], int vaha[], int kapacita){
	int m[pocet + 1][kapacita + 1];
	int i, w;
	// m[i][w] - i je počet, w je kapacita
	// m[0][w] - 0 věcí, nulová váha
	for(w=0; w<=kapacita; w++){
		m[0][w]=0;
	}
    for(i=0;i<=pocet;i++){
            m[i][0]=0;
    }
	for(i=1; i<=pocet; i++){
		for(w=1; w<=kapacita; w++){
			m[i][w] = m[i-1][w];
			if (vaha[i] <= w){
				//projdu postupně výsledky podle toho jak vyjdou hodnoty a vyberu tu větší
				m[i][w] = std::max(m[i-1][w] , m[i-1][w-vaha[i]]+cena[i]);
			}
		}
	}
	return m[pocet][kapacita];
}

int main(){
	int Cena[] = {0 ,3 , 2 , 3, 10};
	int Vaha[] = {0 ,1 , 11, 9, 6 };
	int Kapacita = 14;
	std::cout << batoh(4, Cena, Vaha, Kapacita) << std::endl;
}